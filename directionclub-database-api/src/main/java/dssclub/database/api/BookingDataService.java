package dssclub.database.api;

import java.time.LocalDate;
import java.util.List;

import dssclub.core.model.Booking;

public interface BookingDataService {

	public List<Booking> findAll();

	public boolean create(Booking booking);

	public void deleteById(Long id);

	public boolean update(Booking booking);

	public List<Booking> getTotalBookingsForGivenDay(LocalDate date);

}
