package dssclub.database.api;

import java.util.List;

import dssclub.core.model.User;

public interface UserDataService {

	public List<User> getUsers();

}
