package dssclub.database.api;

import java.util.List;

import dssclub.core.model.SportType;

public interface SportTypeDataService {

	public void create(SportType sportType);

	public List<SportType> findAll();

}
