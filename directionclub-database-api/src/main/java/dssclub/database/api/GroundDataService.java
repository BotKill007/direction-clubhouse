package dssclub.database.api;

import java.util.List;

import dssclub.core.model.Ground;

public interface GroundDataService {

	public void create(Ground ground);

	public List<Ground> findAll();

}
