package dssclub.database.api;

import java.util.List;

import dssclub.core.model.Sport;

public interface SportDataService {

	List<Sport> getAllSports();

	public void create(Sport sport);

}
