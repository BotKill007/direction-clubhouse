package dssclub.database.api;

import java.util.List;

import dssclub.core.model.Equipment;

public interface EquipmentDataService {

	public void create(Equipment equipment);

	public List<Equipment> findAll();

}
