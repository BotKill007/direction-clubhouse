package dssclub.usecase;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import dssclub.api.exception.bill.BillException;
import dssclub.api.model.BillResultVO;
import dssclub.api.model.BookingDetailVO;
import dssclub.api.model.EquipmentVO;
import dssclub.api.model.GroundVO;
import dssclub.api.model.SportVO;
import dssclub.api.service.bill.GenerateBill;

//@RunWith(MockitoJUnitRunner.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class GenerateBillTest {

	@Autowired
	private GenerateBill generateBill;

	@Test
	public void generateBillTest_PASS() {
		SportVO sport = SportVO.builder().active(true).available(true).equipmentVOs(null).groundVOs(null)
				.name("Cricket").sportTypeVO(null).build();
		GroundVO ground = GroundVO.builder().active(true).available(true).id(12L).price(4800L).pricePerHours(1200L)
				.name("SV ground").build();
		EquipmentVO eqVo = EquipmentVO.builder().active(true).available(true).id(21L).name("Cricket Ball").price(12L)
				.quantity(30L).brandName("MRF").build();
		Set<EquipmentVO> equipments = new HashSet<>();
		equipments.add(eqVo);
		BookingDetailVO booking = new BookingDetailVO(sport, ground, equipments, LocalDate.now(), LocalTime.now(),
				LocalTime.now().plusHours(2));
		booking.setId(1L);
		try {
			BillResultVO vo = generateBill.generateBill(booking);
			assertEquals(new Long(720), vo.getEquipmentAmount());
			assertEquals(new Long(2400), vo.getGroundAmount());
			assertEquals(new Long(3120), vo.getTotalAmount());
		} catch (BillException e) {
			e.printStackTrace();
		}
	}

}
