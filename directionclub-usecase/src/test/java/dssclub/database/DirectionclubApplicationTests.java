package dssclub.database;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import dssclub.usecase.DirectionclubApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DirectionclubApplication.class)
public class DirectionclubApplicationTests {

	@Test
	public void contextLoads() {
	}

}
