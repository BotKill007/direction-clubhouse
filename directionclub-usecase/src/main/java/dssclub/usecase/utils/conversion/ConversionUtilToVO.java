package dssclub.usecase.utils.conversion;

import static java.util.Objects.isNull;

import dssclub.api.model.BillResultVO;
import dssclub.core.model.bill.BillResult;

public class ConversionUtilToVO {

	public static BillResultVO convertBillResult(BillResult billResult) {
		if (isNull(billResult))
			return null;
		return BillResultVO.builder().groundAmount(billResult.getGroundAmount())
				.equipmentAmount(billResult.getEquipmentAmount()).totalAmount(billResult.getTotalAmount()).build();
	}

}
