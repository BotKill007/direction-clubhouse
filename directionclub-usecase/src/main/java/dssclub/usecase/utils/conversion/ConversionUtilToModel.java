package dssclub.usecase.utils.conversion;

import static java.util.Objects.isNull;

import java.util.HashSet;
import java.util.Set;

import dssclub.api.model.BookingDetailVO;
import dssclub.api.model.EquipmentVO;
import dssclub.api.model.GroundVO;
import dssclub.core.model.Booking;
import dssclub.core.model.Equipment;
import dssclub.core.model.Ground;

public class ConversionUtilToModel {

	public static Booking convertBookingVO(BookingDetailVO bookingVO) {
		return Booking.builder().id(bookingVO.getId()).date(bookingVO.getDate()).startTime(bookingVO.getStartTime())
				.endTime(bookingVO.getEndTime()).ground(convertGroundVO(bookingVO.getGround()))
				.equipments(convertSetOfBookingEquipmentVO(bookingVO.getEquipments())).build();
	}

	private static Set<Equipment> convertSetOfBookingEquipmentVO(Set<EquipmentVO> equipmentsVO) {
		Set<Equipment> equipments = new HashSet<Equipment>();
		if (isNull(equipmentsVO))
			return equipments;
		for (EquipmentVO equipmentVO : equipmentsVO) {
			equipments.add(convertEquipmentVO(equipmentVO));
		}
		return equipments;
	}

	private static Equipment convertEquipmentVO(EquipmentVO equipmentVO) {
		return Equipment.builder().id(equipmentVO.getId()).brandName(equipmentVO.getBrandName())
				.price(equipmentVO.getPrice()).quantity(equipmentVO.getQuantity()).build();
	}

	private static Ground convertGroundVO(GroundVO groundVO) {
		return Ground.builder().id(groundVO.getId()).location(groundVO.getLocation()).capacity(groundVO.getCapacity())
				.price(groundVO.getPrice()).pricePerHours(groundVO.getPricePerHours()).available(groundVO.isAvailable())
				.build();
	}

}
