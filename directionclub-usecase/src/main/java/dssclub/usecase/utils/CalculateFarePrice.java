package dssclub.usecase.utils;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

import dssclub.core.model.Booking;
import dssclub.core.model.Equipment;
import dssclub.core.model.Ground;

public class CalculateFarePrice {

	public static long calculateFareForEquipments(Set<Equipment> equipments, LocalTime startTime, LocalTime endTime) {
		if (isNull(equipments))
			return 0;
		long equipmentAmount = 0l;
		for (Equipment equipment : equipments) {
			long totalAmountPerEquipment = calculateFareBasedOnQuantity(
					calculateFareBasedOnTotalHours(equipment.getPrice(), startTime, endTime), equipment.getQuantity());
			equipmentAmount += totalAmountPerEquipment;
		}
		return equipmentAmount;
	}

	public static Long calculateFareBasedOnTotalHours(Long pricePerHours, LocalTime startTime, LocalTime endTime) {
		long totalNumberOfHours = startTime.until(endTime, ChronoUnit.HOURS);
		return pricePerHours * totalNumberOfHours;
	}

	public static Long calculateFareBasedOnQuantity(Long price, Long quantity) {
		return price * quantity;
	}

	public static Long calculateTotalGroundAmont(List<Ground> grounds) {
		if (isNull(grounds))
			return 0L;
		return grounds.stream().mapToLong(ground -> ground.getPrice()).sum();
	}

	public static Long calculateTotalEquipmentAmount(List<Equipment> equipments) {
		if (isNull(equipments))
			return 0L;
		return equipments.stream().mapToLong(eq -> (eq.getPrice() * eq.getQuantity())).sum();
	}

	public static Long calculateTotalBookingAmount(List<Booking> bookings) {
		if (isNull(bookings))
			return 0L;
		return bookings.stream().mapToLong(booking -> calculateTotalAmountFromBooking(booking)).sum();
	}

	private static Long calculateTotalAmountFromBooking(Booking booking) {
		return calculateTotalEquipmentAmount(booking.getEquipments().stream().collect(toList()))
				+ booking.getGround().getPrice();
	}

}
