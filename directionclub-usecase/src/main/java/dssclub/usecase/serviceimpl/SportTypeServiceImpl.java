package dssclub.usecase.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.api.model.SportTypeVO;
import dssclub.api.service.SportTypeService;
import dssclub.core.model.SportType;
import dssclub.database.api.SportTypeDataService;

@Transactional
@Service
public class SportTypeServiceImpl implements SportTypeService {

	@Resource
	private SportTypeDataService sportTypeDataService;

	@Override
	public List<SportTypeVO> findAllGridData() {
		List<SportType> sportTypes = sportTypeDataService.findAll();
		return sportTypes.stream().map(f1 -> convertToVO(f1)).collect(Collectors.toList());
	}

	private SportTypeVO convertToVO(SportType sportType) {
		return SportTypeVO.builder().id(sportType.getId()).name(sportType.getName()).build();
	}

	@Override
	public void create(SportTypeVO sportTypeVO) {
		sportTypeDataService.create(convertToEntity(sportTypeVO));

	}

	private SportType convertToEntity(SportTypeVO vo) {
		return SportType.builder().id(vo.getId()).name(vo.getName()).build();
	}

}
