package dssclub.usecase.serviceimpl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.api.model.SportGridVO;
import dssclub.api.model.SportVO;
import dssclub.api.service.SportService;
import dssclub.core.model.Equipment;
import dssclub.core.model.Ground;
import dssclub.core.model.Sport;
import dssclub.core.model.SportEquipment;
import dssclub.core.model.SportGround;
import dssclub.database.api.SportDataService;
import dssclub.usecase.beanconverters.SportDtoConverter;

@Transactional
@Service
public class SportServiceImpl implements SportService {

	@Resource
	private SportDataService sportDataService;

	@Override
	public List<SportGridVO> findAllGridData() {
		List<Sport> allSports = sportDataService.getAllSports();
		return allSports.stream().map(f1 -> convertToSportGridVO(f1)).collect(Collectors.toList());
	}

	private SportGridVO convertToSportGridVO(Sport sport) {

		SportGridVO sportGridVO = SportGridVO.builder().build();
		sportGridVO.setId(sport.getId());
		sportGridVO.setName(sport.getName());
		sportGridVO.setSporttypeName(sport.getSportType().getName());

		List<String> grounds = sport.getSportGrounds().stream().map(SportGround::getGround).filter(Objects::nonNull)
				.map(Ground::getName).collect(Collectors.toList());
		sportGridVO.setGround(String.join(", ", grounds));

		List<String> equipments = sport.getSportEquipments().stream().map(SportEquipment::getEquipment)
				.filter(Objects::nonNull).map(Equipment::getName).collect(Collectors.toList());
		sportGridVO.setEquipment(String.join(", ", equipments));

		return sportGridVO;
	}

	@Override
	public void create(SportVO sportVO) {
		sportDataService.create(SportDtoConverter.convertToDto(sportVO));
	}

	@Override
	public List<SportVO> findAll() {
		List<Sport> allSports = sportDataService.getAllSports();
		List<SportVO> result = allSports.stream().map(f1 -> SportDtoConverter.convertToVO(f1))
				.collect(Collectors.toList());
		return result;
	}

}
