package dssclub.usecase.serviceimpl;

import static dssclub.usecase.utils.CalculateFarePrice.calculateTotalBookingAmount;
import static dssclub.usecase.utils.CalculateFarePrice.calculateTotalEquipmentAmount;
import static dssclub.usecase.utils.CalculateFarePrice.calculateTotalGroundAmont;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import dssclub.api.model.BillResultVO;
import dssclub.api.service.BookingCollectionService;
import dssclub.core.model.Booking;
import dssclub.core.model.Equipment;
import dssclub.core.model.Ground;
import dssclub.database.api.BookingDataService;

@Service
public class BookingCollectionServiceImpl implements BookingCollectionService {

	@Resource
	private BookingDataService bookingDataService;

	@Override
	public BillResultVO getTotalEarningsOfGivenDay(LocalDate date) {
		List<Booking> bookings = bookingDataService.getTotalBookingsForGivenDay(date);
		List<Equipment> equipments = bookings.stream().flatMap(booking -> booking.getEquipments().stream())
				.collect(Collectors.toList());
		List<Ground> grounds = bookings.stream().map(booking -> booking.getGround()).collect(Collectors.toList());
		return BillResultVO.builder().equipmentAmount(calculateTotalEquipmentAmount(equipments))
				.groundAmount(calculateTotalGroundAmont(grounds)).totalAmount(calculateTotalBookingAmount(bookings))
				.build();
	}

}
