package dssclub.usecase.serviceimpl;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.api.model.EquipmentVO;
import dssclub.api.service.EquipmentService;
import dssclub.core.model.Equipment;
import dssclub.database.api.EquipmentDataService;

@Transactional
@Service
public class EquipmentServiceImpl implements EquipmentService {

	@Resource
	private EquipmentDataService equipmentDataService;

	@Override
	public void create(EquipmentVO equipmentVO) {
		Equipment entity = convertToEntity(equipmentVO);
		equipmentDataService.create(entity);
	}

	@Override
	public List<EquipmentVO> findAll() {
		List<Equipment> list = equipmentDataService.findAll();
		return list.stream().map(f1 -> convertToVO(f1)).collect(toList());
	}

	@Override
	public List<EquipmentVO> findAvailableEquipments() {
		List<Equipment> list = equipmentDataService.findAll();
		return list.stream().filter(Equipment::isAvailable).map(this::convertToVO).collect(toList());
	}

	private Equipment convertToEntity(EquipmentVO equipmentVO) {
		return Equipment.builder().id(equipmentVO.getId()).name(equipmentVO.getName())
				.brandName(equipmentVO.getBrandName()).price(equipmentVO.getPrice()).quantity(equipmentVO.getQuantity())
				.available(equipmentVO.isAvailable()).build();
	}

	private EquipmentVO convertToVO(Equipment equipment) {
		return EquipmentVO.builder().id(equipment.getId()).name(equipment.getName()).brandName(equipment.getBrandName())
				.price(equipment.getPrice()).quantity(equipment.getQuantity()).available(equipment.isAvailable())
				.build();
	}

}
