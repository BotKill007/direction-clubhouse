package dssclub.usecase.serviceimpl;

import static java.util.Objects.nonNull;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.api.model.GroundVO;
import dssclub.api.service.GroundService;
import dssclub.core.model.Ground;
import dssclub.database.api.GroundDataService;

@Transactional
@Service
public class GroundServiceImpl implements GroundService {

	@Resource
	private GroundDataService groundDataService;

	private final LocalTime priceTime = LocalTime.of(16, 59, 00);

	@Override
	public void create(GroundVO groundVO) {
		groundDataService.create(convertToDto(groundVO));
	}

	private Ground convertToDto(GroundVO groundVO) {
		return Ground.builder().id(groundVO.getId()).name(groundVO.getName()).active(groundVO.isActive())
				.available(groundVO.isAvailable()).capacity(groundVO.getCapacity()).location(groundVO.getLocation())
				.price(groundVO.getPrice()).pricePerHours(groundVO.getPricePerHours()).build();
	}

	@Override
	public List<GroundVO> findAll() {
		List<Ground> resultList = groundDataService.findAll();
		return resultList.stream().map(f1 -> convertToViewObject(f1)).collect(Collectors.toList());
	}

	@Override
	public List<GroundVO> findAvailableGrounds() {
		List<Ground> resultList = groundDataService.findAll();
		return resultList.stream().filter(Ground::isAvailable).map(this::convertToViewObject)
				.collect(Collectors.toList());
	}

	@Override
	public GroundVO getGroundPriceByTime(GroundVO groundVO, LocalTime startTime, LocalTime endTime) {
		if (nonNull(groundVO) && nonNull(startTime) && nonNull(endTime)) {
			// FIXME price time is being set hardcoded. Need to work on keeping
			// it flexible?
			if (startTime.isAfter(priceTime))
				// FIXME This is hardcoded. How to get value directly from db.
				// If price changes after specific time
				groundVO.setPricePerHours(new Long(1500));
			groundVO.setPrice(groundVO.getPricePerHours() * (ChronoUnit.HOURS.between(startTime, endTime)));
		}
		System.out.println(groundVO);
		return groundVO;
	}

	private GroundVO convertToViewObject(Ground ground) {
		return GroundVO.builder().id(ground.getId()).name(ground.getName()).active(ground.isActive())
				.available(ground.isAvailable()).capacity(ground.getCapacity()).location(ground.getLocation())
				.price(ground.getPrice()).pricePerHours(ground.getPricePerHours()).build();
	}

}
