package dssclub.usecase.serviceimpl.bill;

import static dssclub.usecase.utils.CalculateFarePrice.calculateFareBasedOnTotalHours;
import static dssclub.usecase.utils.CalculateFarePrice.calculateFareForEquipments;
import static dssclub.usecase.utils.conversion.ConversionUtilToModel.convertBookingVO;
import static dssclub.usecase.utils.conversion.ConversionUtilToVO.convertBillResult;

import org.springframework.stereotype.Service;

import dssclub.api.exception.bill.BillException;
import dssclub.api.model.BillResultVO;
import dssclub.api.model.BookingDetailVO;
import dssclub.api.service.bill.GenerateBill;
import dssclub.core.model.Booking;
import dssclub.core.model.bill.BillResult;
import dssclub.usecase.model.validator.BookingValidator;

@Service
public class GenerateBillImpl implements GenerateBill {

	@Override
	public BillResultVO generateBill(BookingDetailVO bookingVO) throws BillException {
		Booking booking = convertBookingVO(bookingVO);
		BookingValidator.validateBooking(booking);
		BillResult billResult = calculateBill(booking);
		return convertBillResult(billResult);
	}

	private BillResult calculateBill(Booking booking) {
		BillResult billResult = BillResult.builder().id(booking.getId()).user(booking.getUser())
				.groundAmount(calculateFareBasedOnTotalHours(booking.getGround().getPricePerHours(),
						booking.getStartTime(), booking.getEndTime()))
				.equipmentAmount(calculateFareForEquipments(booking.getEquipments(), booking.getStartTime(),
						booking.getEndTime()))
				.build();
		billResult.setTotalAmount(billResult.getGroundAmount() + billResult.getEquipmentAmount());
		return billResult;
	}

}
