package dssclub.usecase.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.api.model.BookingVO;
import dssclub.api.service.BookingService;
import dssclub.core.model.Booking;
import dssclub.database.api.BookingDataService;
import dssclub.usecase.beanconverters.BookingConverterUtil;

@Transactional
@Service
public class BookingServiceImpl implements BookingService {

	@Resource
	private BookingDataService bookingDataService;

	@Override
	public List<BookingVO> findAll() {
		List<Booking> bookings = bookingDataService.findAll();
		List<BookingVO> result = bookings.stream().map(f1 -> BookingConverterUtil.convertToVO(f1))
				.collect(Collectors.toList());
		return result;
	}

	@Override
	public boolean create(BookingVO bookingVO) {
		Booking booking = BookingConverterUtil.convertToDto(bookingVO);
		bookingDataService.create(booking);
		return true;
	}

	@Override
	public void deleteById(Long id) {
		bookingDataService.deleteById(id);
	}

	@Override
	public boolean update(BookingVO bookingVO) {
		Booking booking = BookingConverterUtil.convertToDto(bookingVO);
		bookingDataService.update(booking);
		return true;
	}

}
