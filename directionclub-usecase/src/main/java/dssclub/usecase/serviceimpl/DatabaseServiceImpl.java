package dssclub.usecase.serviceimpl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.api.service.DatabaseService;
import dssclub.database.api.DataService;

@Transactional
@Service
public class DatabaseServiceImpl implements DatabaseService {

	@Resource
	private DataService dataService;

	@Override
	public void setup() {
		dataService.setup();
	}

}
