package dssclub.usecase.jerseyconfig;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;

import dssclub.api.jerseyconfig.LocalDateConverterProvider;
import dssclub.api.jerseyconfig.ObjectMapperProvider;
import dssclub.api.jerseyconfig.ZonedDateTimeParamConverterProvider;

@Configuration
@PropertySource("classpath:/default-jersey.properties")
public class JerseyServlet extends ResourceConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(JerseyServlet.class);

	@Resource
	private ApplicationContext context;

	public JerseyServlet() {
		// empty
	}

	@PostConstruct
	public void postConstruct() {
		JerseyServlet.LOGGER.info("Starting Rest-Service context");

		for (final Map.Entry<String, Object> entry : this.context.getBeansWithAnnotation(Service.class).entrySet()) {
			JerseyServlet.LOGGER.info("Registered Rest-Service " + entry.getValue().getClass());
			register(entry.getValue());
		}
		register(ObjectMapperProvider.class);
		register(LocalDateConverterProvider.class);
		register(ZonedDateTimeParamConverterProvider.class);
		register(SerializationFeature.class);
		register(DeserializationFeature.class);
		property(ServletProperties.FILTER_FORWARD_ON_404, true);

	}
}