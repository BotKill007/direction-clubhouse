package dssclub.usecase.beanconverters;

import java.util.List;
import java.util.stream.Collectors;

import dssclub.api.model.BookingEquipmentVO;
import dssclub.api.model.BookingGroundVO;
import dssclub.api.model.BookingVO;
import dssclub.api.model.BookingVO2;
import dssclub.api.model.UserVO;
import dssclub.core.model.Booking;
import dssclub.core.model.BookingEquipment;
import dssclub.core.model.BookingGround;
import dssclub.core.model.User;

public class BookingConverterUtil {

	public static BookingVO convertToVO(Booking booking) {
		return BookingVO.builder().id(booking.getId()).amountPaid(booking.isAmountPaid())
				.startDate(booking.getStartDate()).endDate(booking.getEndDate())
				.userVO(convertToUserVO(booking.getUser()))
				.bookingEquipments(convertToBookingEquipments(booking.getBookingEquipments()))
				.bookingGrounds(convertToBookingGrounds(booking.getBookingGrounds())).build();
	}

	private static List<BookingGroundVO> convertToBookingGrounds(List<BookingGround> bookingGrounds) {
		return bookingGrounds.stream().map(f1 -> getBookingGround(f1)).collect(Collectors.toList());
	}

	private static BookingGroundVO getBookingGround(BookingGround bookingGround) {
		return BookingGroundVO.builder().id(bookingGround.getId())
				.groundVO(GroundDtoConverter.convertToVO(bookingGround.getGround())).build();
	}

	private static List<BookingEquipmentVO> convertToBookingEquipments(List<BookingEquipment> bookingEquipments) {
		List<BookingEquipmentVO> list = bookingEquipments.stream().map(f1 -> getBookingEquipment(f1))
				.collect(Collectors.toList());
		return list;
	}

	private static BookingEquipmentVO getBookingEquipment(BookingEquipment bookingEquipment) {
		return BookingEquipmentVO.builder().id(bookingEquipment.getId())
				.equipmentVO(EquipmentDtoConverter.convertToVO(bookingEquipment.getEquipment())).build();
	}

	private static UserVO convertToUserVO(User user) {
		return UserVO.builder().id(user.getId()).name(user.getName()).build();
	}

	public static Booking convertToDto(BookingVO dto) {
		return Booking.builder().id(dto.getId()).amountPaid(dto.isAmountPaid()).startDate(dto.getStartDate())
				.endDate(dto.getEndDate())
				.bookingEquipments(convertToBookingEquipmentEntities(dto.getBookingEquipments()))
				.bookingGrounds(convertToBookingGroundEntities(dto.getBookingGrounds())).build();
	}

	public static Booking convertToDto(BookingVO2 dto) {
		return Booking.builder().id(dto.getId()).date(dto.getDate()).startTime(dto.getStartTime())
				.endTime(dto.getEndTime()).ground(GroundDtoConverter.convertToDto(dto.getBookingGround()))
				.equipments(EquipmentDtoConverter.convertToSetDTO(dto.getBookingEquipments())).build();
	}

	private static List<BookingGround> convertToBookingGroundEntities(List<BookingGroundVO> bookingGrounds) {
		return bookingGrounds.stream().map(f1 -> getBookingGroundEntity(f1)).collect(Collectors.toList());
	}

	private static BookingGround getBookingGroundEntity(BookingGroundVO vo) {
		return BookingGround.builder().id(vo.getId()).ground(GroundDtoConverter.convertToDto(vo.getGroundVO())).build();
	}

	private static List<BookingEquipment> convertToBookingEquipmentEntities(
			List<BookingEquipmentVO> bookingEquipments) {
		return bookingEquipments.stream().map(f1 -> getBookingEquipmentEntity(f1)).collect(Collectors.toList());
	}

	private static BookingEquipment getBookingEquipmentEntity(BookingEquipmentVO dto) {
		return BookingEquipment.builder().id(dto.getId())
				.equipment(EquipmentDtoConverter.convertToDto(dto.getEquipmentVO())).build();
	}

}
