package dssclub.usecase.beanconverters;

import dssclub.api.model.GroundVO;
import dssclub.core.model.Ground;

public class GroundDtoConverter {

	public static Ground convertToDto(GroundVO vo) {
		return Ground.builder().id(vo.getId()).name(vo.getName()).location(vo.getLocation()).capacity(vo.getCapacity())
				.pricePerHours(vo.getPricePerHours()).available(vo.isAvailable()).build();
	}

	public static GroundVO convertToVO(Ground dto) {
		return GroundVO.builder().id(dto.getId()).name(dto.getName()).capacity(dto.getCapacity())
				.location(dto.getLocation()).pricePerHours(dto.getPricePerHours()).available(dto.isAvailable()).build();
	}

}
