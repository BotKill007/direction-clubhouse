package dssclub.usecase.beanconverters;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import dssclub.api.model.EquipmentVO;
import dssclub.core.model.Equipment;

public class EquipmentDtoConverter {

	public static Equipment convertToDto(EquipmentVO vo) {
		return Equipment.builder().id(vo.getId()).name(vo.getName()).brandName(vo.getBrandName()).price(vo.getPrice())
				.quantity(vo.getQuantity()).available(vo.isAvailable()).build();
	}

	public static EquipmentVO convertToVO(Equipment dto) {
		return EquipmentVO.builder().id(dto.getId()).name(dto.getName()).brandName(dto.getBrandName())
				.available(dto.isAvailable()).quantity(dto.getQuantity()).price(dto.getPrice()).build();
	}

	public static Set<Equipment> convertToSetDTO(Set<EquipmentVO> bookingEquipments) {
		if (bookingEquipments == null)
			return new HashSet<>();
		return bookingEquipments.stream().map(EquipmentDtoConverter::convertToDto).collect(toSet());
	}

}
