package dssclub.usecase.beanconverters;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dssclub.api.model.SportEquipmentVO;
import dssclub.api.model.SportGroundVO;
import dssclub.api.model.SportTypeVO;
import dssclub.api.model.SportVO;
import dssclub.core.model.Sport;
import dssclub.core.model.SportEquipment;
import dssclub.core.model.SportGround;
import dssclub.core.model.SportType;

public class SportDtoConverter {

	public static Sport convertToDto(SportVO vo) {
		return Sport.builder().id(vo.getId()).name(vo.getName())
				.sportType(
						SportType.builder().id(vo.getSportTypeVO().getId()).name(vo.getSportTypeVO().getName()).build())
				.sportEquipments(convertToSportEquipmentDtos(vo.getSportEquipmentVOs()))
				.sportGrounds(convertToSporGroundDtos(vo.getSportGroundVOs())).build();
	}

	private static List<SportGround> convertToSporGroundDtos(Set<SportGroundVO> sportGroundVOs) {
		List<SportGround> sportGrounds = new ArrayList<>();
		for (SportGroundVO vo : sportGroundVOs) {
			sportGrounds.add(SportGround.builder().id(vo.getId())
					.ground(GroundDtoConverter.convertToDto(vo.getGroundVO())).build());
		}
		return sportGrounds;
	}

	private static List<SportEquipment> convertToSportEquipmentDtos(Set<SportEquipmentVO> sportEquipmentVOs) {
		List<SportEquipment> sportEquipments = new ArrayList<>();
		for (SportEquipmentVO vo : sportEquipmentVOs) {
			sportEquipments.add(SportEquipment.builder().id(vo.getId())
					.equipment(EquipmentDtoConverter.convertToDto(vo.getEquipmentVO())).build());
		}
		return sportEquipments;
	}

	public static SportVO convertToVO(Sport sport) {
		return SportVO.builder().id(sport.getId()).name(sport.getName())
				.sportTypeVO(SportTypeVO.builder().id(sport.getSportType().getId()).name(sport.getSportType().getName())
						.build())
				.sportGroundVOs(convertToSportGroundVOs(sport.getSportGrounds()))
				.sportEquipmentVOs(convertToSportEquipmentVOs(sport.getSportEquipments())).build();
	}

	private static Set<SportEquipmentVO> convertToSportEquipmentVOs(List<SportEquipment> sportEquipments) {
		Set<SportEquipmentVO> result = new HashSet<>();
		for (SportEquipment dto : sportEquipments) {
			result.add(SportEquipmentVO.builder().id(dto.getId())
					.equipmentVO(EquipmentDtoConverter.convertToVO(dto.getEquipment())).build());
		}
		return result;
	}

	private static Set<SportGroundVO> convertToSportGroundVOs(List<SportGround> sportGrounds) {
		Set<SportGroundVO> result = new HashSet<>();
		for (SportGround dto : sportGrounds) {
			result.add(SportGroundVO.builder().id(dto.getId()).groundVO(GroundDtoConverter.convertToVO(dto.getGround()))
					.build());
		}
		return result;
	}

}
