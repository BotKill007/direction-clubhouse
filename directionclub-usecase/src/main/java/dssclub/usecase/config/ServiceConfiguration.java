package dssclub.usecase.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import dssclub.database.api.BookingDataService;
import dssclub.database.api.DataService;
import dssclub.database.api.EquipmentDataService;
import dssclub.database.api.GroundDataService;
import dssclub.database.api.SportDataService;
import dssclub.database.api.SportTypeDataService;
import dssclub.database.serviceimpl.BookingDataServiceImpl;
import dssclub.database.serviceimpl.DataServiceImpl;
import dssclub.database.serviceimpl.EquipmentDataServiceImpl;
import dssclub.database.serviceimpl.GroundDataServiceImpl;
import dssclub.database.serviceimpl.SportDataServiceImpl;
import dssclub.database.serviceimpl.SportTypeDataServiceImpl;

@Configuration
@ComponentScan("dssclub.usecase")
public class ServiceConfiguration {

	@Bean
	public SportDataService sportDataService() {
		return new SportDataServiceImpl();
	}

	@Bean
	public DataService dataService() {
		return new DataServiceImpl();
	}

	@Bean
	public EquipmentDataService equipmentDataService() {
		return new EquipmentDataServiceImpl();
	}

	@Bean
	public GroundDataService groundDataService() {
		return new GroundDataServiceImpl();
	}

	@Bean
	public SportTypeDataService sportTypeDataService() {
		return new SportTypeDataServiceImpl();
	}

	@Bean
	public BookingDataService bookingDataService() {
		return new BookingDataServiceImpl();
	}

}
