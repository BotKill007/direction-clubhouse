package dssclub.usecase.model.validator;

import static java.util.Objects.isNull;

import dssclub.api.exception.bill.BillException;
import dssclub.core.model.Booking;

public class BookingValidator {

	public static void validateBooking(Booking booking) throws BillException {
		if (isNull(booking))
			throw new BillException("No booking found");
		if (isNull(booking.getId()))
			throw new BillException("Booking Id is empty");
		/*if (isNull(booking.getUser()))
			throw new BillException("User not present for given booking");*/
		if (isNull(booking.getGround()))
			throw new BillException("No ground selected for given booking");
	}
}
