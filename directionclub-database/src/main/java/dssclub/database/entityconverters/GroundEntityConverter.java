package dssclub.database.entityconverters;

import dssclub.core.model.Ground;
import dssclub.database.entities.GroundEntity;

public class GroundEntityConverter {

	public static GroundEntity convertToEntity(Ground ground) {
		return GroundEntity.builder().id(ground.getId()).name(ground.getName()).capacity(ground.getCapacity())
				.available(ground.isAvailable()).location(ground.getLocation()).pricePerHours(ground.getPricePerHours())
				.build();
	}

	public static Ground convertToDto(GroundEntity entity) {
		return Ground.builder().id(entity.getId()).name(entity.getName()).location(entity.getLocation())
				.capacity(entity.getCapacity()).pricePerHours(entity.getPricePerHours()).build();
	}

}
