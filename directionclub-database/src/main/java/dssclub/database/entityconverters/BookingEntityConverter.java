package dssclub.database.entityconverters;

import java.util.List;
import java.util.stream.Collectors;

import dssclub.core.model.Booking;
import dssclub.core.model.BookingEquipment;
import dssclub.core.model.BookingGround;
import dssclub.database.entities.BookingEntity;
import dssclub.database.entities.BookingEquipmentEntity;
import dssclub.database.entities.BookingGroundEntity;

public class BookingEntityConverter {

	public static BookingEntity convertToEntity(Booking booking) {
		return BookingEntity.builder().id(booking.getId()).amountPaid(booking.isAmountPaid())
				.startDate(booking.getStartDate()).endDate(booking.getEndDate())
				.bookingEquipmentEntities(convertToBookingEquipmentEntities(booking.getBookingEquipments()))
				.bookingGroundEntities(convertToBookingGroundEntities(booking.getBookingGrounds())).build();
	}

	private static List<BookingGroundEntity> convertToBookingGroundEntities(List<BookingGround> bookingGrounds) {
		return bookingGrounds.stream().map(f1 -> convertToBookingGroundEntity(f1)).collect(Collectors.toList());
	}

	private static BookingGroundEntity convertToBookingGroundEntity(BookingGround dto) {
		return BookingGroundEntity.builder().id(dto.getId())
				.groundEntity(GroundEntityConverter.convertToEntity(dto.getGround())).build();
	}

	private static List<BookingEquipmentEntity> convertToBookingEquipmentEntities(
			List<BookingEquipment> bookingEquipments) {
		return bookingEquipments.stream().map(f1 -> convertToBookingEquipmentEntity(f1)).collect(Collectors.toList());
	}

	private static BookingEquipmentEntity convertToBookingEquipmentEntity(BookingEquipment dto) {
		return BookingEquipmentEntity.builder().id(dto.getId())
				.equipmentEntity(EquipmentEntityConverter.convertToEntity(dto.getEquipment())).build();
	}

	public static Booking convertToDto(BookingEntity entity) {
		return Booking.builder().id(entity.getId()).amountPaid(entity.isAmountPaid()).startDate(entity.getStartDate())
				.endDate(entity.getEndDate()).bookingGrounds(convertToGroundDtos(entity.getBookingGroundEntities()))
				.bookingEquipments(convertToBookingEquipmentDtos(entity.getBookingEquipmentEntities())).build();
	}

	private static List<BookingEquipment> convertToBookingEquipmentDtos(
			List<BookingEquipmentEntity> bookingEquipmentEntities) {
		return bookingEquipmentEntities.stream().map(f1 -> convertToBookingEquipmentDTO(f1))
				.collect(Collectors.toList());
	}

	private static BookingEquipment convertToBookingEquipmentDTO(BookingEquipmentEntity entity) {
		return BookingEquipment.builder().id(entity.getId())
				.equipment(EquipmentEntityConverter.convertToDto(entity.getEquipmentEntity())).build();
	}

	private static List<BookingGround> convertToGroundDtos(List<BookingGroundEntity> bookingGroundEntities) {
		return bookingGroundEntities.stream().map(f1 -> convertToBookingGroundDTO(f1)).collect(Collectors.toList());
	}

	private static BookingGround convertToBookingGroundDTO(BookingGroundEntity entity) {
		return BookingGround.builder().id(entity.getId())
				.ground(GroundEntityConverter.convertToDto(entity.getGroundEntity())).build();
	}

}
