package dssclub.database.entityconverters;

import dssclub.core.model.Equipment;
import dssclub.database.entities.EquipmentEntity;

public class EquipmentEntityConverter {

	public static EquipmentEntity convertToEntity(Equipment dto) {
		return EquipmentEntity.builder().id(dto.getId()).name(dto.getName()).brandName(dto.getBrandName())
				.available(dto.isAvailable()).quantity(dto.getQuantity()).price(dto.getPrice()).build();
	}

	public static Equipment convertToDto(EquipmentEntity entity) {
		return Equipment.builder().id(entity.getId()).name(entity.getName()).brandName(entity.getBrandName())
				.quantity(entity.getQuantity()).price(entity.getPrice()).available(entity.isAvailable()).build();
	}

}
