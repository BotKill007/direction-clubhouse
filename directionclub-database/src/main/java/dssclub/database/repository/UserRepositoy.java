package dssclub.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dssclub.core.model.User;

@Repository
public interface UserRepositoy extends JpaRepository<User, Long> {

}
