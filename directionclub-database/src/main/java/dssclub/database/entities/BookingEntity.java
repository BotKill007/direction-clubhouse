package dssclub.database.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "DC_BOOKING")
public class BookingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DC_BOOKING_ID")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DC_USER_ID")
	private UserEntity userEntity;

	@JoinColumn(name = "START_DATE")
	private Date startDate;

	@JoinColumn(name = "END_DATE")
	private Date endDate;

	@OneToMany(orphanRemoval = true, mappedBy = "bookingEntity", cascade = CascadeType.ALL)
	private List<BookingGroundEntity> bookingGroundEntities;

	@OneToMany(orphanRemoval = true, mappedBy = "bookingEntity", cascade = CascadeType.ALL)
	private List<BookingEquipmentEntity> bookingEquipmentEntities;

	@JoinColumn(name = "AMOUNT_PAID")
	private boolean amountPaid;

}
