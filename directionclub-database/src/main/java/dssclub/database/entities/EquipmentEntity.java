package dssclub.database.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "DC_EQUIPMENT")
public class EquipmentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DC_EQUIPMENT_ID")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "BRAND_NAME")
	private String brandName;

	@Column(name = "PRICE")
	private Long price;

	@Column(name = "QUANTITY")
	private Long quantity;

	@Column(name = "AVAILABLE")
	private boolean available;

}
