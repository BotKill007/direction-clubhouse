package dssclub.database.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "DC_SPORT")
public class SportEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DC_SPORT_ID")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DC_SPORT_TYPE_ID")
	private SportTypeEntity sportTypeEntity;

	@OneToMany(orphanRemoval = true, mappedBy = "sportEntity", cascade = CascadeType.ALL)
	private List<SportGroundEntity> sportGroundEntities;

	@OneToMany(orphanRemoval = true, mappedBy = "sportEntity", cascade = CascadeType.ALL)
	private List<SportEquipmentEntity> sportEquipmentEntities;

}
