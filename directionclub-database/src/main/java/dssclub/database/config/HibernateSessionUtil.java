package dssclub.database.config;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import dssclub.database.entities.BookingEntity;
import dssclub.database.entities.BookingEquipmentEntity;
import dssclub.database.entities.BookingGroundEntity;
import dssclub.database.entities.EquipmentEntity;
import dssclub.database.entities.GroundEntity;
import dssclub.database.entities.MemberShipTypeEntity;
import dssclub.database.entities.MembershipEntity;
import dssclub.database.entities.SportEntity;
import dssclub.database.entities.SportEquipmentEntity;
import dssclub.database.entities.SportGroundEntity;
import dssclub.database.entities.SportTypeEntity;
import dssclub.database.entities.UserEntity;

public class HibernateSessionUtil {
	private static SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			if (sessionFactory == null) {
				Configuration cfg = new Configuration().configure("hibernate.cfg.xml")
						.addAnnotatedClass(BookingEntity.class).addAnnotatedClass(BookingEquipmentEntity.class)
						.addAnnotatedClass(BookingGroundEntity.class).addAnnotatedClass(EquipmentEntity.class)
						.addAnnotatedClass(GroundEntity.class).addAnnotatedClass(SportEntity.class)
						.addAnnotatedClass(SportEquipmentEntity.class).addAnnotatedClass(SportGroundEntity.class)
						.addAnnotatedClass(SportTypeEntity.class).addAnnotatedClass(UserEntity.class)
						.addAnnotatedClass(MembershipEntity.class).addAnnotatedClass(MemberShipTypeEntity.class);
				final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
						.applySettings(cfg.getProperties()).build();
				sessionFactory = cfg.buildSessionFactory(registry);
			}
			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static Session getSession() throws HibernateException {

		Session retSession = null;
		try {
			retSession = sessionFactory.openSession();
		} catch (Throwable t) {
			System.err.println("Exception while getting session.. ");
			t.printStackTrace();
		}
		if (retSession == null) {
			System.err.println("session is discovered null");
		}
		return retSession;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}
}
