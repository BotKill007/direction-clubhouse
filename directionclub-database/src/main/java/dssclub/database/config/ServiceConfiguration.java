package dssclub.database.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("dssclub.database.*")
// @EnableAutoConfiguration
// @PropertySource("classpath:application.properties")
public class ServiceConfiguration {

	// @Value("${spring.datasource.driver-class-name}")
	// private String driver;
	//
	// @Bean
	// public DataSource dataSource() {
	// DriverManagerDataSource dataSource = new DriverManagerDataSource();
	//
	// dataSource.setDriverClassName(driver);
	// dataSource.setUsername("mysqluser");
	// dataSource.setPassword("mysqlpass");
	// dataSource.setUrl("jdbc:mysql://localhost:3306/myDb?createDatabaseIfNotExist=true");
	//
	// return dataSource;
	// }

}
