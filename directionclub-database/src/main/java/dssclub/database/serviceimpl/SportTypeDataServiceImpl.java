package dssclub.database.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.core.model.SportType;
import dssclub.database.api.SportTypeDataService;
import dssclub.database.config.HibernateSessionUtil;
import dssclub.database.entities.SportTypeEntity;

@Transactional
@Service
public class SportTypeDataServiceImpl implements SportTypeDataService {

	@Override
	public void create(SportType sportType) {
		Session session = HibernateSessionUtil.getSession();
		Transaction t = session.beginTransaction();
		SportTypeEntity sportTypeEntity = convertToEntity(sportType);
		session.persist(sportTypeEntity);
		t.commit();
	}

	private SportTypeEntity convertToEntity(SportType sportType) {
		return SportTypeEntity.builder().id(sportType.getId()).name(sportType.getName())
				.available(sportType.isAvailable()).build();
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<SportType> findAll() {
		Session session = HibernateSessionUtil.getSession();
		List<SportTypeEntity> entities = session.createCriteria(SportTypeEntity.class).list();
		return entities.stream().map(f1 -> convertToDto(f1)).collect(Collectors.toList());
	}

	private SportType convertToDto(SportTypeEntity entity) {
		return SportType.builder().id(entity.getId()).name(entity.getName()).available(entity.getAvailable()).build();
	}

}
