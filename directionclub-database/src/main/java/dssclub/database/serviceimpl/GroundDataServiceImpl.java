package dssclub.database.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.core.model.Ground;
import dssclub.database.api.GroundDataService;
import dssclub.database.config.HibernateSessionUtil;
import dssclub.database.entities.GroundEntity;

@Transactional
@Service
public class GroundDataServiceImpl implements GroundDataService {

	@Override
	public void create(Ground ground) {
		Session session = HibernateSessionUtil.getSession();
		Transaction t = session.beginTransaction();
		GroundEntity groundEntity = convertToEntity(ground);
		session.persist(groundEntity);
		t.commit();
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Ground> findAll() {
		Session session = HibernateSessionUtil.getSession();
		List<GroundEntity> entities = session.createCriteria(GroundEntity.class).list();
		return entities.stream().map(f1 -> convertToDto(f1)).collect(Collectors.toList());
	}

	public GroundEntity convertToEntity(Ground ground) {
		return GroundEntity.builder().id(ground.getId()).available(ground.isAvailable()).capacity(ground.getCapacity())
				.location(ground.getLocation()).name(ground.getName()).price(ground.getPrice())
				.pricePerHours(ground.getPricePerHours()).build();
	}

	public Ground convertToDto(GroundEntity groundEntity) {
		return Ground.builder().id(groundEntity.getId()).available(groundEntity.isAvailable())
				.capacity(groundEntity.getCapacity()).location(groundEntity.getLocation()).name(groundEntity.getName())
				.price(groundEntity.getPrice()).pricePerHours(groundEntity.getPricePerHours()).build();
	}

}
