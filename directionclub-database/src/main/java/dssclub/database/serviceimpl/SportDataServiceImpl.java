package dssclub.database.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.core.model.Equipment;
import dssclub.core.model.Ground;
import dssclub.core.model.Sport;
import dssclub.core.model.SportEquipment;
import dssclub.core.model.SportGround;
import dssclub.core.model.SportType;
import dssclub.database.api.SportDataService;
import dssclub.database.config.HibernateSessionUtil;
import dssclub.database.entities.EquipmentEntity;
import dssclub.database.entities.GroundEntity;
import dssclub.database.entities.SportEntity;
import dssclub.database.entities.SportEquipmentEntity;
import dssclub.database.entities.SportGroundEntity;
import dssclub.database.entities.SportTypeEntity;

@Transactional
@Service
public class SportDataServiceImpl implements SportDataService {

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Sport> getAllSports() {
		Session session = HibernateSessionUtil.getSession();
		List<SportEntity> entities = session.createCriteria(SportEntity.class).list();
		return entities.stream().map(f1 -> convertToDto(f1)).collect(Collectors.toList());
	}

	@Override
	public void create(Sport sport) {
		Session session = HibernateSessionUtil.getSession();
		Transaction t = session.beginTransaction();
		SportEntity sportEntity = convertToEntity(sport);
		session.persist(sportEntity);
		t.commit();
	}

	public SportEntity convertToEntity(Sport dto) {

		SportEntity entity = SportEntity.builder().id(dto.getId()).name(dto.getName())
				.sportTypeEntity(SportTypeEntity.builder().id(dto.getSportType().getId()).build()).build();

		entity.setSportGroundEntities(convertToGroundEntities(entity, dto.getSportGrounds()));
		entity.setSportEquipmentEntities(convertToEquipmentEntities(entity, dto.getSportEquipments()));
		return entity;
	}

	private List<SportEquipmentEntity> convertToEquipmentEntities(SportEntity entity,
			List<SportEquipment> sportEquipments) {
		List<SportEquipmentEntity> sportEquipmentEntities = new ArrayList<>();
		for (SportEquipment sportEquipment : sportEquipments) {
			sportEquipmentEntities.add(SportEquipmentEntity.builder().id(sportEquipment.getId()).sportEntity(entity)
					.equipmentEntity(EquipmentEntity.builder().id(sportEquipment.getEquipment().getId()).build())
					.build());
		}
		return sportEquipmentEntities;
	}

	private List<SportGroundEntity> convertToGroundEntities(SportEntity entity, List<SportGround> sportGrounds) {
		List<SportGroundEntity> sportGroundEntities = new ArrayList<>();
		for (SportGround sportGround : sportGrounds) {
			sportGroundEntities.add(SportGroundEntity.builder().id(sportGround.getId()).sportEntity(entity)
					.groundEntity(GroundEntity.builder().id(sportGround.getGround().getId()).build()).build());
		}
		return sportGroundEntities;
	}

	public Sport convertToDto(SportEntity entity) {
		return Sport.builder().id(entity.getId()).name(entity.getName())
				.sportType(convertToSportTypeDto(entity.getSportTypeEntity()))
				.sportEquipments(convertToEquipmentDtos(entity.getSportEquipmentEntities()))
				.sportGrounds(convertToGroundDtos(entity.getSportGroundEntities())).build();
	}

	private List<SportGround> convertToGroundDtos(List<SportGroundEntity> sportGroundEntities) {
		List<SportGround> grounds = new ArrayList<>();
		for (SportGroundEntity entity : sportGroundEntities) {
			grounds.add(SportGround.builder().id(entity.getId()).ground(convertToGroundDto(entity.getGroundEntity()))
					.build());
		}
		return grounds;
	}

	private Ground convertToGroundDto(GroundEntity entity) {
		return Ground.builder().id(entity.getId()).name(entity.getName()).build();
	}

	private List<SportEquipment> convertToEquipmentDtos(List<SportEquipmentEntity> sportEquipmentEntities) {
		List<SportEquipment> equipments = new ArrayList<>();
		for (SportEquipmentEntity entity : sportEquipmentEntities) {
			equipments.add(SportEquipment.builder().id(entity.getId())
					.equipment(convertToEquipmentDto(entity.getEquipmentEntity())).build());
		}
		return equipments;
	}

	private Equipment convertToEquipmentDto(EquipmentEntity entity) {
		return Equipment.builder().id(entity.getId()).name(entity.getName()).build();
	}

	private SportType convertToSportTypeDto(SportTypeEntity entity) {
		return SportType.builder().id(entity.getId()).name(entity.getName()).available(entity.getAvailable()).build();
	}

}
