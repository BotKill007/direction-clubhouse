package dssclub.database.serviceimpl;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.core.model.Booking;
import dssclub.database.api.BookingDataService;
import dssclub.database.config.HibernateSessionUtil;
import dssclub.database.entities.BookingEntity;
import dssclub.database.entityconverters.BookingEntityConverter;

@Transactional
@Service
public class BookingDataServiceImpl implements BookingDataService {

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Booking> findAll() {
		Session session = HibernateSessionUtil.getSession();
		List<BookingEntity> list = session.createCriteria(BookingEntity.class).list();
		return list.stream().map(f1 -> BookingEntityConverter.convertToDto(f1)).collect(Collectors.toList());
	}

	@Override
	public boolean create(Booking booking) {
		Session session = HibernateSessionUtil.getSession();
		BookingEntity entity = BookingEntityConverter.convertToEntity(booking);
		session.persist(entity);
		return true;
	}

	@Override
	public void deleteById(Long id) {
		Session session = HibernateSessionUtil.getSession();
		session.delete(BookingEntity.builder().id(id).build());
	}

	@Override
	public boolean update(Booking booking) {
		Session session = HibernateSessionUtil.getSession();
		BookingEntity entity = BookingEntityConverter.convertToEntity(booking);
		session.persist(entity);
		return true;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Booking> getTotalBookingsForGivenDay(LocalDate date) {
		Session session = HibernateSessionUtil.getSession();
		// FIXME Need to modify booking entity in order for query to execute
		List<BookingEntity> list = session.createCriteria(BookingEntity.class).add(Restrictions.eq("START_DATE", date))
				.list();
		return list.stream().map(BookingEntityConverter::convertToDto).collect(Collectors.toList());
	}

}
