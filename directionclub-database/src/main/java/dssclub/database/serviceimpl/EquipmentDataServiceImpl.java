package dssclub.database.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dssclub.core.model.Equipment;
import dssclub.database.api.EquipmentDataService;
import dssclub.database.config.HibernateSessionUtil;
import dssclub.database.entities.EquipmentEntity;

@Transactional
@Service
public class EquipmentDataServiceImpl implements EquipmentDataService {

	@Override
	public void create(Equipment equipment) {
		Configuration cfg = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(EquipmentEntity.class);
		cfg.configure("hibernate.cfg.xml");
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties())
				.build();
		final SessionFactory sessionFactory = cfg.buildSessionFactory(registry);
		Session session = sessionFactory.openSession();

		Transaction t = session.beginTransaction();
		session.persist(convertToEntity(equipment));
		t.commit();

		System.out.println("successfully saved");

	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Equipment> findAll() {
		Session session = HibernateSessionUtil.getSession();
		List<EquipmentEntity> list = session.createCriteria(EquipmentEntity.class).list();
		return list.stream().map(f1 -> convertToDto(f1)).collect(Collectors.toList());
	}

	private EquipmentEntity convertToEntity(Equipment equipment) {
		EquipmentEntity entity = EquipmentEntity.builder().id(equipment.getId()).name(equipment.getName())
				.brandName(equipment.getBrandName()).price(equipment.getPrice()).quantity(equipment.getQuantity())
				.available(equipment.isAvailable()).build();
		return entity;
	}

	private Equipment convertToDto(EquipmentEntity entity) {
		return Equipment.builder().id(entity.getId()).name(entity.getName()).brandName(entity.getBrandName())
				.price(entity.getPrice()).quantity(entity.getQuantity()).available(entity.isAvailable()).build();
	}

}
