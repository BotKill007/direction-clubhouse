package dssclub.database.serviceimpl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

import dssclub.database.api.DataService;
import dssclub.database.config.HibernateSessionUtil;
import dssclub.database.entities.EquipmentEntity;
import dssclub.database.entities.GroundEntity;

@Service
public class DataServiceImpl implements DataService {

	// @Autowired
	// AdminCountryInfoRepository repository;

	@Override
	public void setup() {
		Session session = HibernateSessionUtil.getSession();
		Transaction t = session.beginTransaction();

		GroundEntity entity = GroundEntity.builder().name("test").available(false).capacity(100L).location("Latur")
				.pricePerHours(500L).build();
		session.persist(entity);

		EquipmentEntity eQEntity = EquipmentEntity.builder().available(false).brandName("MRF").name("BAT").price(100L)
				.quantity(50L).build();
		session.persist(eQEntity);
		t.commit();

		System.out.println("successfully saved");

	}

}
