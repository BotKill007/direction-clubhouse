package dssclub.api.exception.bill;

public class BillException extends Exception {

	private static final long serialVersionUID = 1L;

	public BillException(String message) {
		super(message);
	}

	public BillException(Throwable cause) {
		super(cause);
	}

}
