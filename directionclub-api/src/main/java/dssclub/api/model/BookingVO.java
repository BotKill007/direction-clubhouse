package dssclub.api.model;

import java.util.Date;
import java.util.Set;

public class BookingVO {

	private Long id;
	private UserVO user;
	private Date startDate;
	private Date endDate;
	private Set<GroundVO> bookingGrounds;
	private Set<EquipmentVO> bookingEquipments;
	private boolean amountPaid; 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserVO getUser() {
		return user;
	}

	public void setUser(UserVO user) {
		this.user = user;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Set<GroundVO> getBookingGrounds() {
		return bookingGrounds;
	}

	public void setBookingGrounds(Set<GroundVO> bookingGrounds) {
		this.bookingGrounds = bookingGrounds;
	}

	public Set<EquipmentVO> getBookingEquipments() {
		return bookingEquipments;
	}

	public void setBookingEquipments(Set<EquipmentVO> bookingEquipments) {
		this.bookingEquipments = bookingEquipments;
	}

	public boolean isAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(boolean amountPaid) {
		this.amountPaid = amountPaid;
	}

}
