package dssclub.api.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dssclub.api.common.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SportVO extends NamedEntity {

	private static final long serialVersionUID = 2127845240190788024L;
	private boolean available;
	private SportTypeVO sportTypeVO;
	private Set<SportGroundVO> sportGroundVOs;
	private Set<SportEquipmentVO> sportEquipmentVOs;

	// UI used
	private Set<GroundVO> groundVOs;
	private Set<EquipmentVO> equipmentVOs;

}
