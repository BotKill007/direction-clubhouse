package dssclub.api.model;

import dssclub.api.common.NamedEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class BillResultVO extends NamedEntity {

	private static final long serialVersionUID = 1L;
	private UserVO userVO;
	@Builder.Default
	private Long totalAmount = new Long(0);
	@Builder.Default
	private Long groundAmount = new Long(0);
	@Builder.Default
	private Long equipmentAmount = new Long(0);

}
