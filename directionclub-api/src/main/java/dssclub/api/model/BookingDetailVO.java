package dssclub.api.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import dssclub.api.common.NamedEntity;
import dssclub.api.model.EquipmentVO;
import dssclub.api.model.GroundVO;
import dssclub.api.model.SportVO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingDetailVO extends NamedEntity {

	private static final long serialVersionUID = 1L;
	private SportVO sport;
	private GroundVO ground;
	private Set<EquipmentVO> equipments;
	private LocalDate date;
	private LocalTime startTime;
	private LocalTime endTime;

}
