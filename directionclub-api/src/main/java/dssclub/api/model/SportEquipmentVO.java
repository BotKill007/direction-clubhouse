package dssclub.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SportEquipmentVO {

	private Long id;
	private SportVO sportVO;
	private EquipmentVO equipmentVO;

}
