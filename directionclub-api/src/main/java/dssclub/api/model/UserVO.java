package dssclub.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dssclub.api.common.NamedEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserVO extends NamedEntity {

	private static final long serialVersionUID = 34350280618224558L;

}
