package dssclub.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dssclub.api.common.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EquipmentVO extends NamedEntity {
	private static final long serialVersionUID = 1L;

	private String brandName;
	private Long price;
	private Long quantity;
	private boolean available;
}
