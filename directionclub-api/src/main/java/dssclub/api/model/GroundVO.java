package dssclub.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dssclub.api.common.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroundVO extends NamedEntity {

	private static final long serialVersionUID = -4932109112756075425L;

	private Long price;
	private String location;
	private Long capacity;
	private Long pricePerHours;
	private boolean available;

}
