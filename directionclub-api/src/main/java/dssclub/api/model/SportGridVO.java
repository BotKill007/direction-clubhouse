package dssclub.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dssclub.api.common.NamedEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SportGridVO extends NamedEntity {

	private static final long serialVersionUID = 4360187487052742579L;
	private String sporttypeName;
	private String ground;
	private String equipment;

}
