package dssclub.api.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class BookingVO2 {

	private Long id;
	private UserVO user;
	private LocalDate date;
	private LocalTime startTime;
	private LocalTime endTime;
	private GroundVO bookingGround;
	private Set<EquipmentVO> bookingEquipments;
	private boolean amountPaid;

}
