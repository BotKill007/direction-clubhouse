package dssclub.api.service;

import java.time.LocalDate;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.RequestParam;

import dssclub.api.model.BillResultVO;

@Path("bookings")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface BookingCollectionService {

	@GET
	@Path("/getTotalEarningsOfGivenDay")
	public BillResultVO getTotalEarningsOfGivenDay(@RequestParam("date") LocalDate date);
	
}
