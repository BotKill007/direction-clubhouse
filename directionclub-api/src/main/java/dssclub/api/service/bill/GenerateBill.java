package dssclub.api.service.bill;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dssclub.api.exception.bill.BillException;
import dssclub.api.model.BillResultVO;
import dssclub.api.model.BookingDetailVO;

@Path("bills")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface GenerateBill {

	@GET
	@Path("/generateBill")
	public BillResultVO generateBill(BookingDetailVO booking) throws BillException;

}
