package dssclub.api.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dssclub.api.model.EquipmentVO;

/**
 * @author haridas.kanure
 *
 */
@Path("/equipments")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface EquipmentService {

	@PUT
	@Path("/create")
	public void create(EquipmentVO equipment);

	@GET
	@Path("/findAll")
	public List<EquipmentVO> findAll();

	@GET
	@Path("/findAvailableEquipments")
	public List<EquipmentVO> findAvailableEquipments();

}
