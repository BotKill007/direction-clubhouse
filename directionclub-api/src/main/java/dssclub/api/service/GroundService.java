package dssclub.api.service;

import java.time.LocalTime;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.RequestParam;

import dssclub.api.model.GroundVO;

/**
 * @author haridas.kanure
 *
 */
@Path("/grounds")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface GroundService {

	@PUT
	@Path("/create")
	public void create(GroundVO groundVO);

	@GET
	@Path("/findAll")
	public List<GroundVO> findAll();

	@GET
	@Path("/findAvailableGrounds")
	public List<GroundVO> findAvailableGrounds();

	@GET
	@Path("/getGroundPriceByTime")
	public GroundVO getGroundPriceByTime(@RequestParam("groundVO") GroundVO groundVO, @RequestParam("startTime") LocalTime startTime, @RequestParam("endTime") LocalTime endTime);

}
