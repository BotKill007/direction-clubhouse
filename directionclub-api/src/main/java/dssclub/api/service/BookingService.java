package dssclub.api.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dssclub.api.model.BookingVO;

/**
 * @author haridas.kanure
 *
 */
@Path("bookings")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface BookingService {

	@GET
	@Path("/findAll")
	public List<BookingVO> findAll();

	@GET
	@Path("/create")
	public boolean create(BookingVO bookingVO);

	@GET
	@Path("/delete")
	public void deleteById(Long id);

	@GET
	@Path("/update")
	public boolean update(BookingVO bookingVO);

}
