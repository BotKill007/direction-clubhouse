package dssclub.api.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import dssclub.api.common.Constants;

/**
 * @author haridas.kanure
 *
 */
@Path("database")
@Produces(Constants.JSON_RESPONSE)
@Consumes(Constants.JSON_REQUEST)
public interface DatabaseService {

	@GET
	@Path("/setup")
	public void setup();

}
