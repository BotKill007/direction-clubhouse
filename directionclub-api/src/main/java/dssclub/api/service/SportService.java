package dssclub.api.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dssclub.api.model.SportGridVO;
import dssclub.api.model.SportVO;

/**
 * @author haridas.kanure
 *
 */
@Path("/sports")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface SportService {

	@GET
	@Path("/findAll")
	public List<SportVO> findAll();

	@GET
	@Path("/findAllGridData")
	public List<SportGridVO> findAllGridData();

	@PUT
	@Path("/create")
	public void create(SportVO sportVO);

}
