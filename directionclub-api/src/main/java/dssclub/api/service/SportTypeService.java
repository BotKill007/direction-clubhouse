package dssclub.api.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dssclub.api.model.SportTypeVO;

/**
 * @author haridas.kanure
 *
 */
@Path("/sporttypes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface SportTypeService {

	@GET
	@Path("/findAll")
	public List<SportTypeVO> findAllGridData();

	@PUT
	@Path("/create")
	public void create(SportTypeVO sportTypeVO);

}
