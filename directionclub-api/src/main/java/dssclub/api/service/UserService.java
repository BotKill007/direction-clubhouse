package dssclub.api.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dssclub.api.model.UserVO;

/**
 * @author haridas.kanure
 *
 */
@Path("userService")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface UserService {

	@GET
	@Path("/findAll")
	public List<UserVO> findAll();

	@POST
	@Path("/register")
	public void registration(UserVO userVO);

}
