/**
 *
 */
package dssclub.api.jerseyconfig;

@FunctionalInterface
public interface JaxRsClientFactory {

	<E> E createClient(String uri, Class<E> clazz);

}
