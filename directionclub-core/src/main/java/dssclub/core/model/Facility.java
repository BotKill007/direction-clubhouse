package dssclub.core.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class Facility extends BasicEntity {

	private static final long serialVersionUID = 4083697195002143172L;

	private Date openTime; // in database we are going save only time
	private Date closeTime;

}
