package dssclub.core.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/*
 * INDOOR , OUTDOOR
 * */
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SportType extends BasicEntity {

	private static final long serialVersionUID = 3313127481455841707L;

	private boolean available;
}
