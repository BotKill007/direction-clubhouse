package dssclub.core.model;

import java.util.Date;

public class Membership {

	private Long id;
	private User user;
	private MemberShipType memberShipType;
	private Long duration; // in months
	private Date startDate;
	private Date endDate;
	private Long totalAmount;
	private Long paidAmount;
	private boolean amountPaid;
	private Long accountBalance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public MemberShipType getMemberShipType() {
		return memberShipType;
	}

	public void setMemberShipType(MemberShipType memberShipType) {
		this.memberShipType = memberShipType;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Long paidAmount) {
		this.paidAmount = paidAmount;
	}

	public boolean isAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(boolean amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Long getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(Long accountBalance) {
		this.accountBalance = accountBalance;
	}

}
