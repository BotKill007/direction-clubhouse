package dssclub.core.model.bill;

import dssclub.core.model.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class BillResult {

	private Long id;
	@Builder.Default
	private Long totalAmount = new Long(0);
	@Builder.Default
	private Long groundAmount = new Long(0);
	@Builder.Default
	private Long equipmentAmount = new Long(0);
	private User user;

}
