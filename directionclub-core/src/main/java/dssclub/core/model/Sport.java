package dssclub.core.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class Sport extends BasicEntity {

	private static final long serialVersionUID = 1051871041587025446L;

	private Facility facility;
	private SportType sportType;
	private List<SportGround> sportGrounds;
	private List<SportEquipment> sportEquipments;

}
