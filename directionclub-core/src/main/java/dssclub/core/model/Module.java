package dssclub.core.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Module {

	private Long id;
	private String name;
	private String route;

}
