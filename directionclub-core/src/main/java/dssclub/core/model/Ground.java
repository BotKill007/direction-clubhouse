package dssclub.core.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Ground extends BasicEntity {

	private static final long serialVersionUID = -8921049550667505540L;

	private Long price;
	private String location;
	private Long capacity;
	private Long pricePerHours;
	private boolean available;

}
