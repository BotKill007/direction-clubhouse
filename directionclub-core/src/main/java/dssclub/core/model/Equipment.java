package dssclub.core.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Equipment extends BasicEntity {

	private static final long serialVersionUID = -4177125841003639937L;

	private String brandName;
	private Long price;
	private Long quantity;
	private boolean available;

}
