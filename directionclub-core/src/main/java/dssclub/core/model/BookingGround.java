package dssclub.core.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class BookingGround {

	private Long id;
	private Booking booking;
	private Ground ground;

}
