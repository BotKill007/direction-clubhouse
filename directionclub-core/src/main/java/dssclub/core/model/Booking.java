package dssclub.core.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class Booking {

	private Long id;
	private User user;
	private LocalDate date;
	private LocalTime startTime;
	private LocalTime endTime;
	private Ground ground;
	private Set<Equipment> equipments; 
	private boolean amountPaid;

}
